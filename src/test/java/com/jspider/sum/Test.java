package test.java.com.jspider.sum;

import static org.junit.jupiter.api.Assertions.*;

import main.java.com.jspider.sum.Calculator;

class Test {

	@org.junit.jupiter.api.Test
	void test() {
		assertEquals(11,Calculator.add(5, 6));
		assertEquals(7,Calculator.add(3, 4));
		assertEquals(11,Calculator.add(5, 6));
	}

}
